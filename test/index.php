<!-- 
    Document   : index
    Created on : 27 Jan, 2012, 7:37:18 PM
    Author     : rajat
-->

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style type="text/css">
  div.suggestions {
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    border: 1px solid black;
    position: absolute;
    width:200px;
    }
    div.suggestions div {
    cursor: default;
    padding: 0px 3px;
}
div.suggestions div.current {
    background-color: #3366cc;
    color: white;
}
div.suggestions div.normal{
    background-color:white;
    color:black;
}
        </style>
        <script type="text/javascript">
            var catElem;
            var srTxt;
            //global
            var isSuggestion=false;
            var dispDiv;
            var progressIcon;
            window.onload=init;
            function init(){
                catElem=document.getElementById("x");
                //catElem.onkeyup=loadXMLDoc;
                //catElem.onkeypress=loadXMLDoc;
                catElem.onkeyup=loadXMLDoc;//here rajat
                progressIcon=document.getElementById("sp1");
                progressIcon.style.visibility="hidden";
                dispDiv=document.getElementById("listEmp");
                setPosition(dispDiv);
                dispDiv.style.visibility="hidden";
                
                
            }
            function setPosition(targetElem){
                var x=0;
                var y=0;
                var offsetPointer=document.getElementById("x");
                while(offsetPointer){
                    x+=offsetPointer.offsetLeft;
                    y+=offsetPointer.offsetTop;
                    offsetPointer=offsetPointer.offsetParent;
                }
                //correct for MAC IE
                if(navigator.userAgent.indexOf("Mac")!=-1 && typeof document.body.leftMargin != "undefined"){
                    x +=document.body.leftMargin;
                    y +=document.body.topMargin;
                    
                }
                y=y-catElem.width-10;
                targetElem.style.left = x+"px";
                targetElem.style.top= y+"px";
            }
            //
            //
            //Actually this methods is a listener for onkeydown
            function handKeyUp(evt){
                
                evt=(evt)?evt : event;
                var charCode=(evt.charCode)?evt.charCode : ((evt.which)? evt.which:evt.keyCode);
                if(charCode==27){
                    document.getElementById("listEmp").style.visibility="hidden";
                    //alert("hello:"+charCode);
                    
                }
                
            }
            //
            
            function loadXMLDoc(evt){
               
                var xmlHttp;
                  if(window.XMLHttpRequest )
                    xmlHttp=new XMLHttpRequest();
                else
                    xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
                ////////////////
                xmlHttp.onreadystatechange=function(){
                    if(xmlHttp.readyState==4 && xmlHttp.status==200){
                        var txt=xmlHttp.responseXML;
                        
                        //alert(txt);
                        //add this into a selection
                        //var uu=document.getElementById("sug1");
                        var uu=document.getElementById("listEmp");
                        //uu.style.visibility="hidden";
                        //alert("h1");
                        if(uu)
                            {
                               
                                var childs=uu.childNodes;
                                //alert("No of Nodes:"+uu.childNodes.length);
                                while(uu.firstChild){
                                    uu.removeChild(uu.firstChild);
                                }
                                
                               // alert(str);
                            }
                            
                            progressIcon.style.visibility="hidden";
                            uu.style.visibility="visible";
                        /*
                        var elem=document.createElement("select");
                        elem.id="sug1";
                        */
                       
                       //uu.style.visibility="hidden";
                        var names=txt.getElementsByTagName("N");
                        var elem=document.getElementById("listEmp");
                        
                       
                       var lastElem=elem;
                        //alert("No of Names Passed:"+names.length);
                        for(var i=0;i<names.length;i++){
                            var y=document.createElement("DIV");
                            y.innerHTML=names[i].childNodes[0].nodeValue;
                            y.onmouseover=function(){
                                this.className="current";
                            }
                            y.onmouseout=function(){
                                this.className="normal";
                            }
                            y.onclick=function(){
                                catElem.value=this.innerHTML;
                                elem.style.visibility="hidden";
                            }
                            y.onblur=function(){
                                elem.style.visibility="hidden";
                            }
                            //on key up
                            
                           // y.onkeyup=handKeyUp;
                           catElem.onkeydown=handKeyUp;
                            //y.className="suggestions";
                            elem.appendChild(y);
                            
                            setPosition(y);
                            
                            
                        }
                        //elem.style.visibility="display";
                        isSuggestion=true;
                        }
                        //


                    
                    
            
                }
                /*Handling Escape */
                evt=(evt)?evt : event;
                var charCode=(evt.charCode)?evt.charCode : ((evt.which)? evt.which:evt.keyCode);
                if(charCode==27){
                    document.getElementById("listEmp").style.visibility="hidden";
                    return;
                }
                /*End Of Handling Escape*/
                srTxt=document.getElementById("x").value;
                /*
                if(srTxt.length<1){
                return;
                }
                else{
                    alert(srTxt);
                }*/
                var url="http://localhost:9999/AjaxAutoComplete/getAutoSuggestion";
                var sendText="id="+srTxt.trim();
                xmlHttp.open("POST",url,true);
                //alert(sendText);
                xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                xmlHttp.send(sendText);
                progressIcon.style.visibity="visible";
            }
            //
        </script>
    </head>
    <body>
        <h1>Auto Suggestion Box</h1>
        <form method="POST" action="getAutoSuggestion">
        <fieldset>
            <legend>Employee</legend>

            Name:<input type="text" id="x" name="na" autocomplete="off"/><span id="sp1"><img src="pics/loading3.gif" width="30" height="30" align="middle"/></span><br />       
                        <div id="listEmp" class="suggestions"></div>
        
        <table>
            <tr>
                <td>&nbsp;&nbsp;</td>
                <td>&nbsp;&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;</td>
                <td>&nbsp;&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;</td>
                <td>&nbsp;&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;</td>
                <td>&nbsp;&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;</td>
                <td>&nbsp;&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <input type="submit" value="Submit"/>
                </td>
            </tr>
        </table>
        </fieldset>
        </form>
    </body>
</html>
