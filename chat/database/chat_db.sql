-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 24, 2015 at 10:18 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `chat_db`
--
CREATE DATABASE IF NOT EXISTS `chat_db` DEFAULT CHARACTER SET ascii COLLATE ascii_bin;
USE `chat_db`;

-- --------------------------------------------------------

--
-- Table structure for table `cr_1`
--

CREATE TABLE IF NOT EXISTS `cr_1` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `data` varchar(2000) COLLATE ascii_bin NOT NULL,
  `user_id` int(20) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=ascii COLLATE=ascii_bin AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `cr_2`
--

CREATE TABLE IF NOT EXISTS `cr_2` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `data` varchar(2000) COLLATE ascii_bin NOT NULL,
  `user_id` int(20) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=ascii COLLATE=ascii_bin AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `cr_3`
--

CREATE TABLE IF NOT EXISTS `cr_3` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `data` varchar(2000) COLLATE ascii_bin NOT NULL,
  `user_id` int(20) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=ascii COLLATE=ascii_bin AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `index_chatroom`
--

CREATE TABLE IF NOT EXISTS `index_chatroom` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(2000) COLLATE ascii_bin NOT NULL,
  `status` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `hash` varchar(2000) COLLATE ascii_bin NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=ascii COLLATE=ascii_bin AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `ul_1`
--

CREATE TABLE IF NOT EXISTS `ul_1` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) NOT NULL,
  `data` varchar(2000) COLLATE ascii_bin NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=ascii COLLATE=ascii_bin AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `ul_2`
--

CREATE TABLE IF NOT EXISTS `ul_2` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) NOT NULL,
  `data` varchar(2000) COLLATE ascii_bin NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=ascii COLLATE=ascii_bin AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `ul_3`
--

CREATE TABLE IF NOT EXISTS `ul_3` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) NOT NULL,
  `data` varchar(2000) COLLATE ascii_bin NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=ascii COLLATE=ascii_bin AUTO_INCREMENT=7 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
